from rest_framework import serializers
from musics.models import Music
from django.utils.timezone import now


class MusicSerializer(serializers.ModelSerializer):
    class Meta:
        model = Music
        fields = '__all__'
        #fields = ('id', 'song', 'singer', 'update', 'created')


def get_days_since_created(self, obj):
    return (now() - obj.created).days

class MusicSerializerV1(serializers.ModelSerializer):
    class Meta:
        model = Music
        fields = ('id', 'song', 'singer')

        
            