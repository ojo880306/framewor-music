from django.db import models


# Create your models here.
class Music(models.Model):
    objects = models.Manager()
    song = models.TextField()
    singer = models.TextField()
    update = models.DateTimeField(auto_now=True,)
    created = models.DateTimeField(auto_now_add=True)

    class Meta:
        db_table = "music"
"""
def fun_raw_sql_query(**kwargs):
    singer = kwargs.get('singer')
    if singer:
        result = Music.objects.raw('SELECT * FROM music WHERE singer = %s', [singer])
    else:
        result = Music.objects.raw('SELECT * FROM music')
    return result        
"""    
